package envoy.authz

#user tokens
#admin token user is bob
admin_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYWRtaW4iLCJzdWIiOiJZbTlpIiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1Mzl9.WCxNAveAVAdRCmkpIObOTaSd0AJRECY2Ch2Qdic3kU8"

guest_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZ3Vlc3QiLCJzdWIiOiJZV3hwWTJVPSIsIm5iZiI6MTUxNDg1MTEzOSwiZXhwIjoxNjQxMDgxNTM5fQ.K5DnnbbIOspRbpCr2IKXE9cPVatGOCBrBQobQmBmaeU"

user_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJaRzl1IiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1MzksInJvbGUiOiJ1c2VyIn0.GgTugMp3sv7HfYfbNaeD7izlg4HBh2jre3NG77Sax5w"

fake_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJaRzl1IiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1MzksInJvbGUiOiJmYWtlIn0.srrpiJMW_QFjgd7_Xz5q2iRc_Eo2eTID-eFpn45V0BY"

request_template := {"attributes": {"request": {"http": {
	"method": "%s",
	"path": "%s",
	"headers": {"authorization": "Bearer %s"},
	"body": {"firstname": "%s", "lastname": "OPA"},
}}}}

test_deny_post_self_reg_admin_people {
	method := "POST"
	token := admin_token
	path := "/people"
	name := "bob"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_allow_post_admin_people {
	method := "POST"
	token := admin_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_allow_get_admin_people {
	method := "GET"
	token := admin_token
	path := "/people"
	name := "test"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_allow_delete_admin_people {
	method := "DELETE"
	token := admin_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_allow_post_user_people {
	method := "POST"
	token := user_token
	path := "/people"
	name := "jon"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_allow_get_user_people {
	method := "GET"
	token := user_token
	path := "/people"
	name := "test"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_deny_delete_user_people {
	method := "DELETE"
	token := user_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_deny_post_guest_people {
	method := "POST"
	token := guest_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_allow_get_guest_people {
	method := "GET"
	token := guest_token
	path := "/people"
	name := "test"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	allow with input as request_object with data.authz as authz_data
}

test_deny_delete_guest_people {
	method := "DELETE"
	token := guest_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_deny_post_fake_people {
	method := "POST"
	token := fake_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_allow_get_fake_people {
	method := "GET"
	token := fake_token
	path := "/people"
	name := "test"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_deny_delete_fake_people {
	method := "DELETE"
	token := fake_token
	path := "/people"
	name := "Elliott"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}

test_deny_self_registration_people {
	method := "POST"
	token := admin_token
	path := "/people"
	name := "bob"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not restrict_self_registration with input as request_object with data.authz as authz_data
}

test_allow_self_registration_people {
	method := "POST"
	token := admin_token
	path := "/people"
	name := "tom"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	restrict_self_registration with input as request_object with data.authz as authz_data
}

authz_data = {
	"roles": {
		"admin": [
			"create_person",
			"query_person",
			"delete_person",
		],
		"user": [
			"create_person",
			"query_person",
		],
		"guest": ["query_person"],
	},
	"permissions": {
		"query_person": {
			"method": "GET",
			"resource": "/people",
			"description": "Get individual user or list of users",
		},
		"create_person": {
			"method": "POST",
			"resource": "/people",
			"description": "Create new user",
		},
		"delete_person": {
			"method": "DELETE",
			"resource": "/people",
			"description": "delete a user by id",
		},
	},
}
