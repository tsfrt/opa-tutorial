# OPA Tutorial

This project demonstrates a simple pattern for authorizing resources for a simple web api using OPA. Both policy and unit tests are written in the Rego language.  I am still fairly new to Rego, but I have found OPA to be a powerful abstraction for codifying policy in a centralized architectural component.

The simple web API that the policy supports exposes a `/people` resource that can be used to POST, GET, and DELETE records.  Access to this resource is managed through the use of roles assigned to users, these roles represent an aggregated set of permissions that map to actions a user can take.  A users role is passed to the server within a JWT token upon each request.

As a first step to solving this problem, I modeled the role -> permission mapping in a json structure that can be accessed as a data object in OPA.  I settled on a straight forward set of Permissions that described actions/resources and roles with arrays of permissions.  It is worth mentioning that new permissions/roles can be added or modified without the need to change the policy with this structure (assuming changes fit into this model).

```json
{
    "authz": {
        "roles": {
            "admin": [
                "create_person",
                "query_person",
                "delete_person",
            ],
            "user": [
                "create_person",
                "query_person",
            ],
            "guest": [
                "query_person"
            ],
        },
        "permissions": {
            "query_person": {
                "method": "GET",
                "resource": "/people",
                "description": "Get individual user or list of users",
            },
            "create_person": {
                "method": "POST",
                "resource": "/people",
                "description": "Create new user",
            },
            "delete_person": {
                "method": "DELETE",
                "resource": "/people",
                "description": "delete a user by id",
            },
        },
    }
}
```

With the permissions defined and roles created, I then set out to write policy that can consume this json structure as a means of authorizing individual requests.  For each request, I check the role passed in the JWT token and pull the associated set of permissions.  

```
user_permissions := resolved_permissions {
	user_role := token.payload.role
	perms := data.authz.roles[user_role]
	resolved_permissions := perms
}
```

This rule uses the extracted role value and looks up the permissions currently associated with that role.  This array of permissions is now stored in the user_permissions rule.

Now that we have the current set of permissions, we need to validate that the user has a permission that enables them to act on a resource with a given action (GET, POST, DELETE).

```
has_permission {
	user_permissions
	some i
	perm := user_permissions[i]
	def := data.authz.permissions[perm]
	def.method == http_request.method
	glob.match(def.resource, ["/"], http_request.path)
}
```

This rule iterates over the set of permissions and matches on a permission that meets the criteria of matching the path (/people) and the action (GET, POST, DELETE).  If no matching permission is found, it evaluates to false.

This takes care of the core of the core requirement, but there is also a need to ensure that for POSTs to the `/people` resource, for those with permission, they cannot create a people record with their same name.  

```
restrict_self_registration {
	http_request.method == "POST"
	glob.match("/people", ["/"], http_request.path)
	lower(http_request.body.firstname) != base64url.decode(token.payload.sub)
}

restrict_self_registration {
	http_request.method != "POST"
	glob.match("/people", ["/"], http_request.path)
}
```

This pair of rules enforces an additional check on POST requests to the `/people` resource.  For actions other than POST no additional policy is applied, but for POSTS a check is done from the web request body's firstname field to the name encoded in the JWT token.

Using the [Rego Playground](https://play.openpolicyagent.org/) I have been able to satisfactorily validate this policy for a handful of use cases, but in order to ensure my policy is air tight, I proceeded to implement unit tests.

My unit tests exercise every permutation of GET,DELETE,POST for my 3 current roles.  Also, I do some additional validation of the restrict_self_registration rule and some testing with invalid JWT tokens.

```
tseufert-a01:opa-stuff tseufert$ opa test -v -f pretty authz.rego authz_test.rego  
data.envoy.authz.test_deny_post_self_reg_admin_people: PASS (5.465858ms)
data.envoy.authz.test_allow_post_admin_people: PASS (839.788µs)
data.envoy.authz.test_allow_get_admin_people: PASS (761.483µs)
data.envoy.authz.test_allow_delete_admin_people: PASS (747.708µs)
data.envoy.authz.test_allow_post_user_people: PASS (796.492µs)
data.envoy.authz.test_allow_get_user_people: PASS (729.369µs)
data.envoy.authz.test_deny_delete_user_people: PASS (641.138µs)
data.envoy.authz.test_deny_post_guest_people: PASS (673.985µs)
data.envoy.authz.test_allow_get_guest_people: PASS (755.884µs)
data.envoy.authz.test_deny_delete_guest_people: PASS (1.617682ms)
data.envoy.authz.test_deny_post_fake_people: PASS (690.059µs)
data.envoy.authz.test_allow_get_fake_people: PASS (627.744µs)
data.envoy.authz.test_deny_delete_fake_people: PASS (590.212µs)
data.envoy.authz.test_deny_self_registration_people: PASS (515.87µs)
data.envoy.authz.test_allow_self_registration_people: PASS (539.029µs)
--------------------------------------------------------------------------------
PASS: 15/15
```

In summary, here is the output of my unit tests.  

To help make testing easier, I setup mock data with jwt tokens and a templated https request structure.

```
admin_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYWRtaW4iLCJzdWIiOiJZbTlpIiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1Mzl9.WCxNAveAVAdRCmkpIObOTaSd0AJRECY2Ch2Qdic3kU8"

guest_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZ3Vlc3QiLCJzdWIiOiJZV3hwWTJVPSIsIm5iZiI6MTUxNDg1MTEzOSwiZXhwIjoxNjQxMDgxNTM5fQ.K5DnnbbIOspRbpCr2IKXE9cPVatGOCBrBQobQmBmaeU"

user_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJaRzl1IiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1MzksInJvbGUiOiJ1c2VyIn0.GgTugMp3sv7HfYfbNaeD7izlg4HBh2jre3NG77Sax5w"

fake_token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJaRzl1IiwibmJmIjoxNTE0ODUxMTM5LCJleHAiOjE2NDEwODE1MzksInJvbGUiOiJmYWtlIn0.srrpiJMW_QFjgd7_Xz5q2iRc_Eo2eTID-eFpn45V0BY"

request_template := {"attributes": {"request": {"http": {
	"method": "%s",
	"path": "%s",
	"headers": {"authorization": "Bearer %s"},
	"body": {"firstname": "%s", "lastname": "OPA"},
}}}}
```

From there, I exercised each use case by using the `with as` syntax to supply my mock input and data objects.

```
test_deny_post_self_reg_admin_people {
	method := "POST"
	token := admin_token
	path := "/people"
	name := "bob"
	request := sprintf(json.marshal(request_template), [name, token, method, path])

	request_object := json.unmarshal(request)
	not allow with input as request_object with data.authz as authz_data
}
```

The expected outcome is generally captured in the rule invocation, using `not` in cases where I wouldn't expect policy to allow a request.
