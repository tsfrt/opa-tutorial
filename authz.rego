package envoy.authz

import input.attributes.request.http as http_request

default allow = false

allow {
	is_token_valid
	has_permission
	restrict_self_registration
}

is_token_valid {
	token.valid
	now := time.now_ns() / 1000000000
	token.payload.nbf <= now
	now < token.payload.exp
}

has_permission {
	user_permissions
	some i
	perm := user_permissions[i]
	def := data.authz.permissions[perm]
	def.method == http_request.method
	glob.match(def.resource, ["/"], http_request.path)
}

user_permissions := resolved_permissions {
	user_role := token.payload.role
	perms := data.authz.roles[user_role]
	resolved_permissions := perms
}

restrict_self_registration {
	http_request.method == "POST"
	glob.match("/people", ["/"], http_request.path)
	lower(http_request.body.firstname) != base64url.decode(token.payload.sub)
}

restrict_self_registration {
	http_request.method != "POST"
	glob.match("/people", ["/"], http_request.path)
}

token := {"valid": valid, "payload": payload} {
	[_, encoded] := split(http_request.headers.authorization, " ")
	trace(encoded)
	[valid, _, payload] := io.jwt.decode_verify(encoded, {"secret": "secret"})
}
